package pl.edu.pwsztar;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static pl.edu.pwsztar.ShoppingCartOperation.PRODUCTS_LIMIT;

public class ShoppingCartTest {

    @BeforeEach
    void setUp() {
        shoppingCart = new ShoppingCart();
    }

    static ShoppingCartOperation shoppingCart;

    @Test
    void shouldAddProducts() {
        boolean result = shoppingCart.addProducts("bread", 3, 1);
        assertTrue(result);
    }
    @Test
    void shouldDoNotAddProductIfPriceIsZeroOrLess() {
        boolean result1 = shoppingCart.addProducts("bread", 0, 1);
        boolean result2 = shoppingCart.addProducts("bread", -5, 1);
        assertFalse(result1);
        assertFalse(result2);
    }
    @Test
    void shouldDoNotAddProductIfCartIsFull() {
        shoppingCart.addProducts("bread", 3, PRODUCTS_LIMIT);

        boolean result1 = shoppingCart.addProducts("bread", 3, 1);
        boolean result2 = shoppingCart.addProducts("cheese", 5, 1);

        assertFalse(result1);
        assertFalse(result2);
    }
    @Test
    void shouldIncreaseProductsQuantityIfAddsTheSameAgain() {
        boolean result1 = shoppingCart.addProducts("bread", 3, 1);
        boolean result2 = shoppingCart.addProducts("bread", 3, 1);

        assertTrue(result1);
        assertTrue(result2);
    }
    @Test
    void shouldDoNotMultipleProductsWithTheSameNameButDifferentPrice() {
        boolean result1 = shoppingCart.addProducts("bread", 3, 1);
        boolean result2 = shoppingCart.addProducts("bread", 3, 1);

        assertTrue(result1);
        assertFalse(result2);
    }

    @Test
    void shouldDeleteProduct() {
        shoppingCart.addProducts("bread", 3, 3);

        boolean result1 = shoppingCart.deleteProducts("bread", 1);
        boolean result2 = shoppingCart.deleteProducts("bread", 2);
        boolean result3 = shoppingCart.deleteProducts("bread", 3);
        boolean result4 = shoppingCart.deleteProducts("bread", 4);

        assertTrue(result1);
        assertTrue(result2);
        assertTrue(result3);
        assertFalse(result4);
    }
    @Test
    void shouldDoNotDeleteProductIfDoNotExistsInCart() {
        boolean result = shoppingCart.deleteProducts("bread", 1);

        assertFalse(result);
    }
    @Test
    void shouldGetQuantityOfProduct() {
        shoppingCart.addProducts("bread", 3, 1);
        shoppingCart.addProducts("bread", 3, 5);

        int quantity = shoppingCart.getQuantityOfProduct("bread");

        assertEquals(6, quantity);
    }
    @Test
    void shouldGetSumProductsPrices() {
        shoppingCart.addProducts("bread", 3, 1);
        shoppingCart.addProducts("cheese", 5, 4);

        int sumOfPrices = shoppingCart.getSumProductsPrices();

        assertEquals(23, sumOfPrices);
    }

    @Test
    void shouldGetSumProductsPricesIfCartIsEmpty() {
        int sumOfPrices = shoppingCart.getSumProductsPrices();

        assertEquals(0, sumOfPrices);
    }

    @Test
    void shouldGetProductPrice() {
        shoppingCart.addProducts("bread", 3, 2);

        int price = shoppingCart.getProductPrice("bread");

        assertEquals(3, price);
    }

    @Test
    void shouldGetProductPriceIfProductDoNotExistsInCart() {
        int price = shoppingCart.getProductPrice("bread");

        assertEquals(0, price);
    }

    @Test
    void shouldReturnListOfProductNames() {
        shoppingCart.addProducts("bread", 3, 1);
        shoppingCart.addProducts("chesse", 5, 4);

        List<String> productNames = shoppingCart.getProductsNames();

        assertEquals(2, productNames.size());
        assertEquals("bread", productNames.get(0));
        assertEquals("chesse", productNames.get(1));
    }

    @Test
    void shouldReturnEmptyListOfProductNamesIfCartIsEmpty() {
        List<String> productNames = shoppingCart.getProductsNames();

        assertEquals(0, productNames.size());
    }
}
